import React from 'react';
import './styles.css';
import Counter from './Counter';
import { useSelector } from 'react-redux';

const App = () => {
  const count = useSelector((state) => state.counter.count);
  const voters = [
    'Anthony Sistilli ',
    'Bob Smith',
    'Stephanie Foo',
    'Kevin Ma',
  ];

  return (
    <div className="App">
      <h1>Redux</h1>
      <a href="https://www.youtube.com/watch?v=wcXTCG8zMhY" target="_blank">
        Redux Tutorial
      </a>
      <span> | </span>
      <a href="https://www.youtube.com/watch?v=1K26DIKt3w8" target="_blank">
        Saga Tutorial
      </a>
      <h2>Total votes: {count}</h2>
      {voters.map((voter) => (
        <Counter name={voter} />
      ))}
    </div>
  );
};

export default App;
